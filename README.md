# gdbmi

For controlling GDB from Java via [GDB/MI](https://sourceware.org/gdb/current/onlinedocs/gdb/GDB_002fMI.html#GDB_002fMI).

## A quick word before we begin

This library is largely taken from the work of spencercw in [this repository](https://bitbucket.org/spencercw/ideagdb).
A large part of the current sources, as well as some of my own personal understanding of GDB/MI, comes from there. I
don't take credit for this work. Their name is listed as the first in the COPYING file.

As you can see, a lot of this source code was from quite a few years ago (the copyright listed is from 2013). Most of
the work renovating the sources to more modern Java, as well as performing some unit testing, has been completed as part
of [tag v1.0.0](https://gitlab.com/addison-caleb-sadness/gdb-mi/-/tags/v1.0.0).

For version 2 of the source code, I intend to continue the original work by taking the original concepts and extending
them to a more extensible, maintainable layout.

That being said: this software is _usable_ and available via `maven.addisoncrump.info` under
`info.addisoncrump.vr:gdbmi:1.0.0`, though I do not suggest it at this time. I will be creating and more importantly
signing future releases via Maven Central. Consider this "unstable".

## A look into the future

gdbmi will be renovated into a fully asynchronous GDB/MI client for Java in upcoming releases, shipping with its own
GDB for consistency. For details, please see the
[project board](https://gitlab.com/addison-caleb-sadness/gdb-mi/-/boards).

## When to use this library

If you need control over a debugger from an arbitrary program, use this. For example, I plan to fully integrate this
with [Ghidra](https://github.com/NationalSecurityAgency/ghidra) after version 2 is stable and fully tested.

If you are implementing an IDE (or a plugin for an IDE) or tacking debugging capabilities onto reverse engineering
suites, use this! Unless you're using another language, in which case either write your own or find an appropriate
library. The only reason I'm writing this is because I need it _in Java_ and I refuse to just JNI it away.

## How can I contribute?

In its current state? Probably not with source code, unless you like handling shoddy code. I can change my mind rather
quickly upon discovering new information (as I have found myself doing multiple times while discovering new features of
GDB by accident).

If you are a GDB guru, some advice would be nice. If you're a binary-oriented programmer, please do help me with test
cases, as I don't know all the languages that GDB covers. Just make a pull request and I'll get to it as soon as I can.

If you aren't involved in software development or research, why are you looking at this? Genuinely curious.

If you're a reverse engineer or vulnerability researcher interested in this work, please provide some additional insight
onto how this would best be used by you in a frontend, as this is the focus of this library's implementation.

If you are unable to contribute otherwise but still wish to support my work, you can always
[donate](https://paypal.me/AddisonCrump). I don't get paid for my research and the time dedicated to this exceeds the
amount of time I dedicate to my coursework sometimes, so I would deeply appreciate it.

## Can I watch you struggle with this?

[Yes](https://www.twitch.tv/vintagetrenchcoat): 1800-2100 CST Wednesday, Friday, and Saturdays most weeks.

## What's the license on this?

This software is covered by an MIT license, available in [COPYING](/COPYING). You'll notice both my name and spencercw's
appear on this with two different dates; updates are my work, base code is spencercw's.

I don't really care how you use this; just don't be a jerk! spencercw put a lot of work into this and I'm doing so as
well.

## How do I report vulnerabilities associated with this software?

If you find a vulnerability in this software or any of its dependencies, please notify me via the issues tab. At a later
date, I will set up a more private disclosure system, but as the number of users of this are 1 (me), public disclosure
is acceptable at this time.
