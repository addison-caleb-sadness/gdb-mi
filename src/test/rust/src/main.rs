fn main() {
    println!("5 + 6 = {}", add(5, 6));
}

#[no_mangle]
fn add(a: u32, b: u32) -> u32 {
    a + b
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hello() {
        assert_eq!(11, add(5, 6));
    }
}
