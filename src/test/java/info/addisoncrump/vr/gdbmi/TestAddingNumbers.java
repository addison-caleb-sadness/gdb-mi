package info.addisoncrump.vr.gdbmi;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAddingNumbers {

    @Test
    public void testAddingNumbers() throws IOException, InterruptedException {
        Process proc = Runtime.getRuntime().exec(new String[]{"./adding-numbers"}, new String[]{}, GdbTestUtil.getBinaryFolder().toFile());
        proc.waitFor();
        String line;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream(), StandardCharsets.US_ASCII))) {
            line = reader.readLine();
        }
        assertEquals("5 + 6 = 11", line);
    }

}
