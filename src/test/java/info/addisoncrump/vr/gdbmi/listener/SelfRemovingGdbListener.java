package info.addisoncrump.vr.gdbmi.listener;

import uk.co.cwspencer.gdb.GdbListenerAdapter;

import java.util.concurrent.*;

public class SelfRemovingGdbListener<T> extends GdbListenerAdapter implements Future<T> {
    private final CompletableFuture<T> record;
    private final PluggableGdbListener listener;

    public SelfRemovingGdbListener(PluggableGdbListener listener) {
        this.listener = listener;
        record = new CompletableFuture<>();
        listener.addListener(this);
    }

    protected void complete(T value) {
        this.listener.removeListener(this);
        record.complete(value);
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        this.listener.removeListener(this);
        return record.cancel(mayInterruptIfRunning);
    }

    @Override
    public boolean isCancelled() {
        return record.isCancelled();
    }

    @Override
    public boolean isDone() {
        return record.isDone();
    }

    @Override
    public T get() throws InterruptedException, ExecutionException {
        return record.get();
    }

    @Override
    public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        try {
            return record.get(timeout, unit);
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            this.cancel(true);
            throw ex;
        }
    }
}
