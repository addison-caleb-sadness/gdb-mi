package info.addisoncrump.vr.gdbmi.listener;

import lombok.SneakyThrows;
import lombok.extern.java.Log;
import uk.co.cwspencer.gdb.GdbListener;
import uk.co.cwspencer.gdb.gdbmi.GdbMiResultRecord;
import uk.co.cwspencer.gdb.gdbmi.GdbMiStreamRecord;
import uk.co.cwspencer.gdb.messages.GdbEvent;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.logging.Level;

@Log
public class PrintingGdbListener implements GdbListener {

    @SneakyThrows
    @Override
    public void onGdbError(Throwable ex) {
        ByteArrayOutputStream s = new ByteArrayOutputStream();
        try (PrintStream ps = new PrintStream(s, true, "ascii")) {
            ex.printStackTrace(ps);
        }
        log.warning("GDB Error: " + s.toString("ascii"));
    }

    @Override
    public void onGdbStarted() {
        log.setLevel(Level.FINE);
        log.fine("GDB started.");
    }

    @Override
    public void onGdbCommandSent(String command, long token) {
        log.fine(String.format("GDB command sent: %s%n", command));
    }

    @Override
    public void onGdbEventReceived(GdbEvent event) {
        log.finer(String.format("GDB event received: %s%n", event.toString()));
    }

    @Override
    public void onStreamRecordReceived(GdbMiStreamRecord record) {
        log.finest(record.message);
    }

    @Override
    public void onResultRecordReceived(GdbMiResultRecord record) {
        log.fine(String.format("GDB result record received: %s%n", record.toString()));
    }
}
