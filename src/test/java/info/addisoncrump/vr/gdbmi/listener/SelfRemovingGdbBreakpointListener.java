package info.addisoncrump.vr.gdbmi.listener;

import uk.co.cwspencer.gdb.messages.GdbEvent;
import uk.co.cwspencer.gdb.messages.GdbStoppedEvent;

public class SelfRemovingGdbBreakpointListener extends SelfRemovingGdbListener<GdbStoppedEvent> {
    public SelfRemovingGdbBreakpointListener(PluggableGdbListener listener) {
        super(listener);
    }

    @Override
    public void onGdbEventReceived(GdbEvent event) {
        if (event instanceof GdbStoppedEvent) {
            if (((GdbStoppedEvent) event).reason == GdbStoppedEvent.Reason.BreakpointHit) {
                complete((GdbStoppedEvent) event);
            }
        }
    }
}
