package info.addisoncrump.vr.gdbmi.listener;

import uk.co.cwspencer.gdb.GdbListener;
import uk.co.cwspencer.gdb.gdbmi.GdbMiResultRecord;
import uk.co.cwspencer.gdb.gdbmi.GdbMiStreamRecord;
import uk.co.cwspencer.gdb.messages.GdbEvent;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class PluggableGdbListener implements GdbListener {
    private final List<GdbListener> listeners;

    public PluggableGdbListener() {
        listeners = new CopyOnWriteArrayList<>();
    }

    @Override
    public void onGdbError(Throwable throwable) {
        listeners.forEach(listener -> listener.onGdbError(throwable));
    }

    @Override
    public void onGdbStarted() {
        listeners.forEach(GdbListener::onGdbStarted);
    }

    @Override
    public void onGdbCommandSent(String s, long l) {
        listeners.forEach(listener -> listener.onGdbCommandSent(s, l));
    }

    @Override
    public void onGdbEventReceived(GdbEvent gdbEvent) {
        listeners.forEach(listener -> listener.onGdbEventReceived(gdbEvent));
    }

    @Override
    public void onStreamRecordReceived(GdbMiStreamRecord gdbMiStreamRecord) {
        listeners.forEach(listener -> listener.onStreamRecordReceived(gdbMiStreamRecord));
    }

    @Override
    public void onResultRecordReceived(GdbMiResultRecord gdbMiResultRecord) {
        listeners.forEach(listener -> listener.onResultRecordReceived(gdbMiResultRecord));
    }

    public void addListener(GdbListener listener) {
        listeners.add(listener);
    }

    public void removeListener(GdbListener listener) {
        listeners.remove(listener);
    }
}
