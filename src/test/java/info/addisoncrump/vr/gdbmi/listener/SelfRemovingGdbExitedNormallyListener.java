package info.addisoncrump.vr.gdbmi.listener;

import uk.co.cwspencer.gdb.messages.GdbEvent;
import uk.co.cwspencer.gdb.messages.GdbStoppedEvent;

public class SelfRemovingGdbExitedNormallyListener extends SelfRemovingGdbListener<GdbStoppedEvent> {
    public SelfRemovingGdbExitedNormallyListener(PluggableGdbListener listener) {
        super(listener);
    }

    @Override
    public void onGdbEventReceived(GdbEvent event) {
        if (event instanceof GdbStoppedEvent) {
            if (((GdbStoppedEvent) event).reason == GdbStoppedEvent.Reason.ExitedNormally) {
                complete((GdbStoppedEvent) event);
            }
        }
    }
}
