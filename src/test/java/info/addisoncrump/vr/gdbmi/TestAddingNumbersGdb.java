package info.addisoncrump.vr.gdbmi;

import info.addisoncrump.vr.gdb.messages.sync.GdbDataEvaluationResult;
import info.addisoncrump.vr.gdbmi.listener.PluggableGdbListener;
import info.addisoncrump.vr.gdbmi.listener.PrintingGdbListener;
import info.addisoncrump.vr.gdbmi.listener.SelfRemovingGdbBreakpointListener;
import info.addisoncrump.vr.gdbmi.listener.SelfRemovingGdbExitedNormallyListener;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import uk.co.cwspencer.gdb.Gdb;
import uk.co.cwspencer.gdb.messages.GdbBreakpoint;
import uk.co.cwspencer.gdb.messages.GdbDoneEvent;
import uk.co.cwspencer.gdb.messages.GdbEvent;
import uk.co.cwspencer.gdb.messages.GdbMiMessageConverter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TestAddingNumbersGdb {

    private PluggableGdbListener listener;
    private Gdb<PluggableGdbListener> gdb;

    private Path output;

    @Before
    public void setup() throws IOException {
        this.listener = new PluggableGdbListener();
        listener.addListener(new PrintingGdbListener());
        List<Class<?>> doneEventTypes = GdbMiMessageConverter.getDefaultDoneEventTypes();
        doneEventTypes.add(GdbDataEvaluationResult.class);
        // insert an evaluation result type
        this.gdb = new Gdb<>("/usr/bin/gdb", null, listener,
                new GdbMiMessageConverter(GdbMiMessageConverter.getDefaultEventTypes(), doneEventTypes));
        gdb.start();
        output = GdbTestUtil.getBinaryFolder().resolve("output.txt").toAbsolutePath();
    }

    @Test
    public void testAddingNumbersRun() throws IOException, InterruptedException, ExecutionException, TimeoutException {
        CompletableFuture<GdbEvent> ev;
        SelfRemovingGdbExitedNormallyListener ex = new SelfRemovingGdbExitedNormallyListener(listener);
        listener.addListener(ex);

        ev = new CompletableFuture<>();
        gdb.sendCommand("-file-exec-and-symbols " + GdbTestUtil.getBinaryFolder().resolve("adding-numbers").toAbsolutePath().toString(), ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDoneEvent.class));

        ev = new CompletableFuture<>();
        gdb.sendCommand("run > " + output.toString(), ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDoneEvent.class));

        ex.get();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(output.toFile()), StandardCharsets.US_ASCII))) {
            assertEquals("5 + 6 = 11", reader.readLine()); // output line
        }
    }

    @Test
    public void testAddingNumbersBreakable() throws IOException, ExecutionException, InterruptedException, TimeoutException {
        CompletableFuture<GdbEvent> ev;
        SelfRemovingGdbExitedNormallyListener ex = new SelfRemovingGdbExitedNormallyListener(listener);
        listener.addListener(ex);

        ev = new CompletableFuture<>();
        gdb.sendCommand("-file-exec-and-symbols " + GdbTestUtil.getBinaryFolder().resolve("adding-numbers").toAbsolutePath().toString(), ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDoneEvent.class));

        ev = new CompletableFuture<>();
        gdb.sendCommand("-break-insert add", ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbBreakpoint.class));
        int bpno = ((GdbBreakpoint) ev.get(1, TimeUnit.SECONDS)).number;

        SelfRemovingGdbBreakpointListener bp = new SelfRemovingGdbBreakpointListener(listener);
        listener.addListener(bp);
        ev = new CompletableFuture<>();
        gdb.sendCommand("run > " + output.toString(), ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDoneEvent.class));
        assertEquals(bpno, bp.get().breakpointNumber.intValue());

        ev = new CompletableFuture<>();
        gdb.sendCommand("-exec-continue", ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDoneEvent.class));

        ex.get();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(output.toFile()), StandardCharsets.US_ASCII))) {
            assertEquals("5 + 6 = 11", reader.readLine()); // output line
        }
    }

    @Test
    public void testAddingNumbersEvaluate() throws IOException, ExecutionException, InterruptedException, TimeoutException {
        CompletableFuture<GdbEvent> ev;
        SelfRemovingGdbExitedNormallyListener ex = new SelfRemovingGdbExitedNormallyListener(listener);
        listener.addListener(ex);

        ev = new CompletableFuture<>();
        gdb.sendCommand("-file-exec-and-symbols " + GdbTestUtil.getBinaryFolder().resolve("adding-numbers").toAbsolutePath().toString(), ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDoneEvent.class));

        ev = new CompletableFuture<>();
        gdb.sendCommand("-break-insert main", ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbBreakpoint.class));
        int bpno = ((GdbBreakpoint) ev.get(1, TimeUnit.SECONDS)).number;

        SelfRemovingGdbBreakpointListener bp = new SelfRemovingGdbBreakpointListener(listener);
        listener.addListener(bp);
        ev = new CompletableFuture<>();
        gdb.sendCommand("run > " + output.toString(), ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDoneEvent.class));
        assertEquals(bpno, bp.get().breakpointNumber.intValue());

        ev = new CompletableFuture<>();
        gdb.sendCommand("-data-evaluate-expression ((uint64_t(*)())add)(5,6)", ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDataEvaluationResult.class));
        assertEquals(11, Long.parseLong(((GdbDataEvaluationResult) ev.get(1, TimeUnit.SECONDS)).value));

        ev = new CompletableFuture<>();
        gdb.sendCommand("-exec-continue", ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDoneEvent.class));

        ex.get();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(output.toFile()), StandardCharsets.US_ASCII))) {
            assertEquals("5 + 6 = 11", reader.readLine()); // output line
        }
    }

    @After
    public void tearDown() {
        gdb.close();
    }


}
