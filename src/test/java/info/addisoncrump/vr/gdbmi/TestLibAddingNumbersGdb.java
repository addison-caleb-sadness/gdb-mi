package info.addisoncrump.vr.gdbmi;

import info.addisoncrump.vr.gdb.messages.sync.GdbDataEvaluationResult;
import info.addisoncrump.vr.gdbmi.listener.PluggableGdbListener;
import info.addisoncrump.vr.gdbmi.listener.PrintingGdbListener;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import uk.co.cwspencer.gdb.Gdb;
import uk.co.cwspencer.gdb.messages.GdbDoneEvent;
import uk.co.cwspencer.gdb.messages.GdbEvent;
import uk.co.cwspencer.gdb.messages.GdbMiMessageConverter;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TestLibAddingNumbersGdb {

    private Gdb<PluggableGdbListener> gdb;

    @Before
    public void setup() throws IOException {
        PluggableGdbListener listener = new PluggableGdbListener();
        listener.addListener(new PrintingGdbListener());
        List<Class<?>> doneEventTypes = GdbMiMessageConverter.getDefaultDoneEventTypes();
        doneEventTypes.add(GdbDataEvaluationResult.class);
        // insert an evaluation result type
        this.gdb = new Gdb<>("/usr/bin/gdb", null, listener,
                new GdbMiMessageConverter(GdbMiMessageConverter.getDefaultEventTypes(), doneEventTypes));
        gdb.start();
    }

    @Test
    public void testAddingNumbersEvaluate() throws IOException, ExecutionException, InterruptedException, TimeoutException {
        CompletableFuture<GdbEvent> ev;

        ev = new CompletableFuture<>();
        gdb.sendCommand("-file-exec-and-symbols " + GdbTestUtil.getBinaryFolder().resolve("libadding_numbers.so").toAbsolutePath().toString(), ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDoneEvent.class));

        ev = new CompletableFuture<>();
        gdb.sendCommand("starti", ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDoneEvent.class));

        ev = new CompletableFuture<>();
        gdb.sendCommand("-data-evaluate-expression ((long(*)())add)(5,6)", ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDataEvaluationResult.class));
        assertEquals(11, Long.parseLong(((GdbDataEvaluationResult) ev.get(1, TimeUnit.SECONDS)).value));

        ev = new CompletableFuture<>();
        gdb.sendCommand("stop", ev::complete);
        assertThat(ev.get(1, TimeUnit.SECONDS), Matchers.instanceOf(GdbDoneEvent.class));
    }

    @After
    public void tearDown() {
        gdb.close();
    }


}
