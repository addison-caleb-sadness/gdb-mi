package info.addisoncrump.vr.gdbmi;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@UtilityClass
public class GdbTestUtil {

    private final String[] extractedFiles = {
            "adding-numbers",
            "libadding_numbers.so"
    };

    private Path binFolder;

    @SuppressFBWarnings("LI_LAZY_INIT_UPDATE_STATIC")
    public Path getBinaryFolder() throws IOException {
        if (binFolder != null) {
            return binFolder;
        }
        binFolder = Files.createTempDirectory("gdb-mi-tests");
        binFolder.toFile().deleteOnExit();

        for (String file : extractedFiles) {
            Path filePath = binFolder.resolve(file);
            Files.copy(GdbTestUtil.class.getResourceAsStream("/" + file), filePath);
            if (!filePath.toFile().setExecutable(true)) {
                throw new RuntimeException(String.format("Couldn't mark %s as executable.", file));
            }
        }
        return binFolder;
    }

}
