package uk.co.cwspencer.gdb;

import uk.co.cwspencer.gdb.gdbmi.GdbMiResultRecord;
import uk.co.cwspencer.gdb.gdbmi.GdbMiStreamRecord;
import uk.co.cwspencer.gdb.messages.GdbEvent;

/**
 * An abstract adapter for the GdbListener type, because generally not all of these are going to be used.
 */
public abstract class GdbListenerAdapter implements GdbListener {
    @Override
    public void onGdbError(Throwable ex) {

    }

    @Override
    public void onGdbStarted() {

    }

    @Override
    public void onGdbCommandSent(String command, long token) {

    }

    @Override
    public void onGdbEventReceived(GdbEvent event) {

    }

    @Override
    public void onStreamRecordReceived(GdbMiStreamRecord record) {

    }

    @Override
    public void onResultRecordReceived(GdbMiResultRecord record) {

    }
}
