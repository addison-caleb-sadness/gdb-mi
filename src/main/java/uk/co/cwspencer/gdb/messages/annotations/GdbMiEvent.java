package uk.co.cwspencer.gdb.messages.annotations;

import uk.co.cwspencer.gdb.gdbmi.GdbMiRecord;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation applied to classes which represent messages in GDB/MI messages.
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface GdbMiEvent {
    /**
     * The record type.
     */
    GdbMiRecord.Type recordType();

    /**
     * The event class name(s).
     */
    String[] className();
}
