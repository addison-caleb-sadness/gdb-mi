package uk.co.cwspencer.gdb.gdbmi;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Class representing a stream record from a GDB/MI stream.
 */
@SuppressFBWarnings("URF_UNREAD_PUBLIC_OR_PROTECTED_FIELD")
public class GdbMiStreamRecord extends GdbMiRecord {
    /**
     * The contents of the record.
     */
    public String message;

    /**
     * Constructor.
     *
     * @param type      The record type.
     * @param userToken The user token. May be null.
     */
    public GdbMiStreamRecord(Type type, Long userToken) {
        this.type = type;
        this.userToken = userToken;
    }
}
