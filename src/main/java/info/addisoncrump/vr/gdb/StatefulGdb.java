package info.addisoncrump.vr.gdb;

import info.addisoncrump.vr.gdb.listener.PluggableGdbListener;
import info.addisoncrump.vr.gdb.listener.StrongPluggableGdbListener;
import info.addisoncrump.vr.gdb.listener.WeakPluggableGdbListener;
import lombok.Value;
import uk.co.cwspencer.gdb.Gdb;
import uk.co.cwspencer.gdb.GdbListener;
import uk.co.cwspencer.gdb.messages.GdbEvent;

import java.nio.file.Path;
import java.util.function.Consumer;

/**
 * Gdb controller with finer control over the instance from the programmer's perspective.
 */
public class StatefulGdb extends Gdb<PluggableGdbListener> {
    /**
     * Constructor; prepares GDB.
     *
     * @param gdbPath The path to the GDB executable.
     * @param weak    Whether the listener
     */
    public StatefulGdb(Path gdbPath, boolean weak) throws IllegalArgumentException {
        super(verifyGdbPath(gdbPath).toString(), null, getAppropriateListener(weak));
    }

    private static Path verifyGdbPath(Path gdbPath) {
        return gdbPath;
    }

    private static PluggableGdbListener getAppropriateListener(boolean weak) {
        if (weak) {
            return new WeakPluggableGdbListener();
        }
        return new StrongPluggableGdbListener();
    }

    /**
     * Wrapper method for {@link PluggableGdbListener#addListener(GdbListener)}.
     *
     * @param listener The listener to add to the listeners.
     */
    public void addListener(GdbListener listener) {
        this.getListener().addListener(listener);
    }

    /**
     * Wrapper method for {@link PluggableGdbListener#removeListener(GdbListener)}.
     *
     * @param listener The listener to remove from the listeners.
     */
    public void removeListener(GdbListener listener) {
        this.getListener().removeListener(listener);
    }

    @Value
    private static class PendingCommand {
        String command;
        Consumer<GdbEvent> callback;
    }
}
