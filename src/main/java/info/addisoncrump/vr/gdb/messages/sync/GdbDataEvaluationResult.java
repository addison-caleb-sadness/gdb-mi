package info.addisoncrump.vr.gdb.messages.sync;

import uk.co.cwspencer.gdb.gdbmi.GdbMiValue;
import uk.co.cwspencer.gdb.messages.GdbDoneEvent;
import uk.co.cwspencer.gdb.messages.annotations.GdbMiDoneEvent;
import uk.co.cwspencer.gdb.messages.annotations.GdbMiField;

/**
 * A GDB data evaluation result.
 */
@SuppressWarnings("unused")
@GdbMiDoneEvent(command = "-data-evaluate-expression")
public class GdbDataEvaluationResult extends GdbDoneEvent {
    /**
     * The scalar value of the variable. This should not be relied upon for aggregate types
     * (structs, etc.) or for dynamic variable objects.
     */
    @GdbMiField(name = "value", valueType = GdbMiValue.Type.String)
    public String value;
}
