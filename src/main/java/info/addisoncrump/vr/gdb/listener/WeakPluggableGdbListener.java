package info.addisoncrump.vr.gdb.listener;

import uk.co.cwspencer.gdb.GdbListener;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

public class WeakPluggableGdbListener extends AbstractPluggableGdbListener<Set<GdbListener>> {
    public WeakPluggableGdbListener() {
        super(getWeakSet());
    }

    private static Set<GdbListener> getWeakSet() {
        return Collections.newSetFromMap(new WeakHashMap<>());
    }
}
