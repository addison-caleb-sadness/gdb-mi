package info.addisoncrump.vr.gdb.listener;

import lombok.RequiredArgsConstructor;
import uk.co.cwspencer.gdb.GdbListener;
import uk.co.cwspencer.gdb.gdbmi.GdbMiResultRecord;
import uk.co.cwspencer.gdb.gdbmi.GdbMiStreamRecord;
import uk.co.cwspencer.gdb.messages.GdbEvent;

import java.util.Collection;

@RequiredArgsConstructor
public abstract class AbstractPluggableGdbListener<T extends Collection<GdbListener>> implements PluggableGdbListener {
    private final T listeners;

    @Override
    public void addListener(GdbListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(GdbListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void onGdbError(Throwable ex) {
        for (GdbListener listener : listeners) {
            listener.onGdbError(ex);
        }
    }

    @Override
    public void onGdbStarted() {
        for (GdbListener listener : listeners) {
            listener.onGdbStarted();
        }
    }

    @Override
    public void onGdbCommandSent(String command, long token) {
        for (GdbListener listener : listeners) {
            listener.onGdbCommandSent(command, token);
        }
    }

    @Override
    public void onGdbEventReceived(GdbEvent event) {
        for (GdbListener listener : listeners) {
            listener.onGdbEventReceived(event);
        }
    }

    @Override
    public void onStreamRecordReceived(GdbMiStreamRecord record) {
        for (GdbListener listener : listeners) {
            listener.onStreamRecordReceived(record);
        }
    }

    @Override
    public void onResultRecordReceived(GdbMiResultRecord record) {
        for (GdbListener listener : listeners) {
            listener.onResultRecordReceived(record);
        }
    }
}
