package info.addisoncrump.vr.gdb.listener;

import uk.co.cwspencer.gdb.GdbListener;

public interface PluggableGdbListener extends GdbListener {

    void addListener(GdbListener listener);

    void removeListener(GdbListener listener);

}
