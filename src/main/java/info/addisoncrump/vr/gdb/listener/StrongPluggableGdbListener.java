package info.addisoncrump.vr.gdb.listener;

import uk.co.cwspencer.gdb.GdbListener;

import java.util.HashSet;
import java.util.Set;

public class StrongPluggableGdbListener extends AbstractPluggableGdbListener<Set<GdbListener>> {
    public StrongPluggableGdbListener() {
        super(new HashSet<>());
    }
}
